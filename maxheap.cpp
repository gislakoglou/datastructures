#include "MAXHEAP.h"

using namespace std;

MAXHEAP * MAXHEAP::build(string s){	//build function to read values from .txt file and create the MINHEAP object

    MAXHEAP * object = NULL;
    node * a = NULL;
    std::ifstream file;

    int i = 0;
    int k,l = 0;

    object = new MAXHEAP;
    object->root = NULL;

    file.open(s.c_str(),ios::in);

    if(!file){
       cout<<"Error in opening file!!!"<<endl;
       return 0;
    }

    while(1){
    	file>>i;
    	if(file.eof()) break;
    	if(l == 0){
    		object->root = insert(i,object->root,object);
    		l++;
    		continue;
    	}
    	a = insert(i,object->root,object);
    }

    file.close();

    return object;
}

void MAXHEAP::display(int level, node *ptr, MAXHEAP * a){	//display function of a binary tree
    int i;

    if (ptr!=NULL)
    {

        display(level + 1, ptr->right, a);
        cout<<endl;
        if (ptr == a->root) cout<<"Maxheap Root -> ";
        for (i = 0; i < level && ptr != a->root; i++)
            cout<<"        ";
        cout<<ptr->value;
        display(level + 1, ptr->left, a);
    }
}

node * MAXHEAP::createNode(int x){ // calls the default constructor for constructing members of a node object

	node * p = NULL;

	p = new node(x);

	return p;
}

void MAXHEAP::preorder(node *a){

    if(a != NULL){
    	cout<<"value = "<<a->value<<" Height = "<<a->height<<endl;
        preorder(a->left);
        preorder(a->right);
    }
}

node * MAXHEAP::insert(int i, node *x,MAXHEAP *a){	//insert gets the number i to be inserter in the tree,the node x which is the root of the tree and the minheap object.

	int k = a->size;
	node * temp = NULL;

	if(x == NULL){
		a->root = new node(i);

		a->size = 1;
		a->minimum = i;
		a->capacity = 1;

		return a->root;
	}
	else if(x != NULL){
		if(a->size == a->capacity) a->capacity = a->capacity*2 + 1;
		if(x->left == NULL && x->right == NULL && x->height < defineHeightFromCapacity(a->capacity) - 1){
			x->left = createNode(i);
			x->left->height = x->height + 1;
			a->size = a->size + 1;
			if(a->minimum < i) a->minimum = i;
			if(x->value < x->left->value) swapNodes(x,x->left);

			return x;
		}
		else if(x->right == NULL && x->left != NULL && x->height < defineHeightFromCapacity(a->capacity) - 1){
			x->right = createNode(i);
			x->right->height = x->height + 1;
			a->size = a->size + 1;
			if(a->minimum < i) a->minimum = i;
			if(x->value < x->right->value ) swapNodes(x,x->right);

			return x;
		}
		if(x->left != NULL && x->height <= defineHeightFromCapacity(a->capacity) - 2){
			x->left = insert(i,x->left,a);
			if(x->left == NULL) return NULL;
			if(x->left != NULL && x->right != NULL){
				if(x->left->value > x->value && x->right->value < x->left->value) swapNodes(x,x->left);
				else if(x->right->value > x->value && x->left->value < x->right->value) swapNodes(x,x->right);
			}
		}
		if(k < a->size) return x;
		if(x->right != NULL && x->height <= defineHeightFromCapacity(a->capacity) - 2){
			x->right = insert(i,x->right,a);
			if(x->right == NULL) return NULL;
			if(x->left != NULL && x->right != NULL){
				if(x->left->value > x->value && x->right->value < x->left->value) swapNodes(x,x->left);
				else if(x->right->value > x->value && x->left->value < x->right->value) swapNodes(x,x->right);
			}
		}
		return x;
	}
	return a->root;
}

void MAXHEAP::Delete(int i, node * x, MAXHEAP * y){
	y->minimum = y->capacity - y->size;									//We hold the number of leaves at the moment at y->minimum

	if(x!=NULL){
		if(x->left!=NULL) Delete(y->minimum,x->left,y);
		if(x->right!=NULL) Delete(y->minimum,x->right,y);
		if(x->right == NULL && x->left!=NULL){
			y->root->value = x->left->value;
			deleteNode(x->left);
			x->left = NULL;
			y->size = y->size - 1;
			x = y->root;
			fixMaximum(y->root);
		}
		if(x->right != NULL && x->left != NULL && x->height == defineHeightFromCapacity(y->capacity) - 1 && y->minimum > 2) y->minimum = y->minimum - 2;
		if(x->right != NULL && x->left != NULL && x->height == defineHeightFromCapacity(y->capacity) - 1 && y->minimum == 2){
			y->root->value = x->right->value;
			deleteNode(x->right);
			x->right = NULL;
			y->size = y->size - 1;
			fixMaximum(y->root);
		}
	}
	y->minimum = y->root->value;
}

void MAXHEAP::fixMaximum(node *a){
	if(a->left != NULL && a->right != NULL){
		if(a->left->value < a->right->value){
			if(a->value < a->right->value){
				swap(a->value,a->right->value);
				fixMaximum(a->right);
			}
		}
		if(a->left->value > a->right->value){
			if(a->value < a->left->value){
				swap(a->value,a->left->value);
				fixMaximum(a->left);
			}
		}
	}
	if(a->left != NULL && a->right == NULL)
		if(a->left->value < a->value) swap(a->value,a->left->value);
}

void MAXHEAP::swapNodes(node * x, node * y){
	int temp = 0;

	temp = x->value;
	if(x->left !=NULL && x->left->value == y->value){
		x->value = y->value;
		y->value = temp;
	}
	if(x->right != NULL && x->right->value == y->value){
		x->value = y->value;
		y->value = temp;
	}
}
int MAXHEAP::getsize(MAXHEAP *a){
	return a->size;
}

int MAXHEAP::findMax(MAXHEAP *a){
	return a->minimum;
}

int MAXHEAP::defineHeightFromCapacity(int x){

	int i = 0;

	x = x + 1;
	while(x!=1){
		x = x/2;
		i++;
	}

	return i;
}

int MAXHEAP::extractNumber(string s){

	if(!s.find("DELETE") || !s.find("INSERT") || !s.find("SEARCH")){
		if(s.find("AVLTREE") || s.find("HASHTABLE") || s.find("MAXHEAP") || s.find("MINHEAP")){
			s.erase(0,14);
			return std::stoi(s);
		}
	}
	return 0;
}

void MAXHEAP::deleteALL(node * x){

	if(x != NULL){
		deleteALL(x->left);
		deleteALL(x->right);
		deleteNode(x);
		x=NULL;
	}
}

void MAXHEAP::deleteNode(node * a){

	a->left = NULL;
	a->right = NULL;
	delete a;
	a = NULL;
}








