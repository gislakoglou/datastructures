#include "HASHTABLE.h"

using namespace std;

HASHTABLE * HASHTABLE::build(string s){	//build function to read values from .txt file and create the MINHEAP object

    HASHTABLE * object = NULL;
    std::ifstream file;

    int i = 0, l = 0;

    object = new HASHTABLE;
    for(i = 0; i < hash_division; i++) object->root[i] = NULL;

    file.open(s.c_str(),ios::in);

    if(!file){
       cout<<"Error in opening file!!!"<<endl;
       return 0;
    }

    while(1){
    	file>>i;
    	if(file.eof()) break;
    	if(l == 0){
    		insert(i,object);
    		l++;
    		continue;
    	}
    	insert(i,object);
    }

    file.close();

    return object;
}

void HASHTABLE::insert(int a, HASHTABLE * object){
	int i = 0;

	i = hashFunction(a);

	if(object->root[i] == NULL){
		object->root[i] = new list;
		object->root[i]->next = NULL;
		object->root[i]->value = a;
		object->size = object->size + 1;
	}
	else if(object->root[i] != NULL && search(a,object) == "FAILURE"){
		while(object->root[i]->next != NULL) object->root[i] = object->root[i]->next;
		if(object->root[i]->next == NULL){
			object->root[i]->next = new list;
			object->root[i]->next->next = NULL;
			object->root[i]->next->value = a;
			object->size = object->size + 1;
		}
	}
}

int HASHTABLE::getsize(HASHTABLE * object){
	return object->size;
}

string HASHTABLE::search(int a ,HASHTABLE * object){

	int i = 0;
	list * b = NULL;

	i = hashFunction(a);

	if(object->root[i] == NULL) return "FAILURE";
	else if(object->root[i] != NULL){
		while(object->root[i] != NULL){
			if(object->root[i]->value == a) return "SUCCESS";
			else object->root[i] = object->root[i]->next;
		}
	}
	return "FAILURE";
}

void HASHTABLE::display(HASHTABLE * a){
	int i = 0;

	for(i = 0; i < hash_division; i++){
		if(a->root[i] != NULL)
			cout<<"i = "<<i<<endl;
		while(a->root[i] != NULL){
			cout<<a->root[i]->value<<" "<<endl;
			a->root[i] = a->root[i]->next;
		}
	}
}

int HASHTABLE::hashFunction(int value){
	return value%hash_division;
}

int HASHTABLE::extractNumber(string s){

	if(!s.find("DELETE") || !s.find("INSERT") || !s.find("SEARCH")){
		if(s.find("AVLTREE") || s.find("HASHTABLE") || s.find("MAXHEAP") || s.find("MINHEAP")){
			s.erase(0,14);
			return std::stoi(s);
		}
	}
	return 0;
}

void HASHTABLE::deleteALL(HASHTABLE * a){

	int i = 0;
	list * temp = NULL;

	for(i = 0; i < hash_division; i++){
		while(a->root[i] != NULL){
			temp = a->root[i]->next;
			delete a->root[i];
			a->root[i] = NULL;
			a->root[i] = temp;
		}
		a->root[i] = NULL;
	}
}

