#include "AVL.h"

using namespace std;

AVL* AVL::build(string s){

    AVL * object = NULL;
    node * a = NULL;
    std::ifstream file;

    int i = 0;
    int k,l = 0;

    object = new AVL;
    object->root = NULL;

    file.open(s.c_str(),ios::in);

    if(!file){
       cout<<"Error in opening file!!!"<<endl;
       return 0;
    }

    while(1){
    	file>>i;
    	if(file.eof()) break;
    	if(l == 0){
    		object->root = insert(i,object->root,object);
    		l++;
    		continue;
    	}
    	a = insert(i,object->root,object);
    }

    file.close();

    return object;
}

node * AVL::insert(int i, node *x,AVL *a){		//insert gets the number i to be inserter in the tree,the node x which is the root of the tree and the avl tree object.

	int k = 0;
	node * temp = NULL;

	if(x == NULL){
		a->root = createNode(i);
		a->root->left = NULL;
		a->root->right = NULL;
		a->size = 1;
		a->minimum = i;
		return a->root;
	}
	else if(x != NULL){
		if(x->value > i && x->left == NULL){
			x->left = createNode(i);
			a->minimum = compare(a->minimum,i)?i:(a->minimum);
			a->size = a->size + 1;
			x->left->height = x->height + 1;
			return x->left;
		}
		else if(x->value < i && x->right == NULL){
			x->right = createNode(i);
			a->minimum = compare(a->minimum,i)?i:(a->minimum);
			a->size = a->size + 1;
			x->right->height = x->height + 1;
			return x->right;
		}
		if(x->value > i){
			temp = insert(i,x->left,a);
			if(!(temp->left == NULL) && !(temp->right == NULL)){
				//cout<<"assigning after rotation to "<<x->value<<endl;
				x->left = temp;
				return temp->left;
			}

			if(i < x->left->value && (treeBalance(x->left,k) - treeBalance(x->right,k) >=2) &&  treeBalance(x->right,k) != 0){ // for more than 3 nodes
				if(treeBalance(x->left->left,k) > treeBalance(x->left->right,k)){
					//cout<<" 1 leftleft more than 3 nodes with node inserted "<<x->value<<endl;
					x = rightRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
				if(treeBalance(x->left->left,k) > treeBalance(x->left->right,k)){
					//cout<<"2 leftright more than 3 nodes with node inserted "<<x->value<<endl;
					x = leftRightRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
			}
			if((treeBalance(x->left,k) - treeBalance(x->right,k) >=2) &&  treeBalance(x->right,k) == 0){ // for 3 nodes chain
				if(x->left->value > i){
					//cout<<"3 leftleft 3 nodes with node inserted = "<<x->value<<endl;
					x = rightRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
				if(x->left->value < i){
					//cout<<"4 leftright 3 nodes with node inserted = "<<x->value<<endl;
					x = leftRightRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
			}
			return temp;
		}
		else if(x->value < i){
			temp = insert(i,x->right,a);
			if(!(temp->left == NULL) && !(temp->right == NULL)){
				//cout<<"assigning after rotation to "<<x->value<<endl;
				x->right = temp;
				return temp->right;
			}

			if(i > x->right->value && (treeBalance(x->right,k) - treeBalance(x->left,k) >=2) &&  treeBalance(x->left,k) != 0){// for more than 3
				if(treeBalance(x->right->right,k) > treeBalance(x->right->left,k)){
					//cout<<"5 rightright more than 3 nodes with node inserted "<<x->value<<endl;
					x = leftRotate(x);
					if(x->right->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
				if(treeBalance(x->right->right,k) < treeBalance(x->right->left,k)){
					//cout<<"6 leftright more than 3 nodes with node inserted "<<x->value<<endl;
					x = rightLeftRotate(x);
					if(x->right->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
			}
			if((treeBalance(x->right,k) - treeBalance(x->left,k) >=2) &&  treeBalance(x->left,k) == 0){// for 3 nodes chain
				if(x->right->value < i){
					//cout<<"7 rightright 3 nodes with node inserted "<<x->value<<endl;
					x = leftRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
				if(x->right->value > i){
					//cout<<"8 leftright 3 nodes with node inserted "<<x->value<<endl;
					x = rightLeftRotate(x);
					if(x->height == 0) a->root = x;
					display(2,x,a);
					cout<<endl;
					return x;
				}
			}
			return temp;
		}
	}
	return a->root;
}

node * AVL::Delete(int i, node *x, AVL * a){

	node *temp = NULL;


	if(x->value == a->root->value && x->left == NULL && x->right == NULL){			//single root node
		delete a->root;
		return NULL;
	}
	if(x->left!=NULL)
		if(x->left->value == i && x->left->left == NULL && x->left->right == NULL){			//deleting a leaf-node
			deleteNode(x->left);
			x->left = NULL;
			a->size = a->size - 1;
			return x;
		}
	if(x->right!=NULL)
		if(x->right->value == i && x->right->left == NULL && x->right->right == NULL){			//deleting a leaf-node
			temp = x->left;
			x->left = NULL;
			delete temp;
			a->size = a->size - 1;
			return x;
		}
	if(x->left->value == i || x->right->value == i){					//deleting nodes who have 1 child
		if(x->left->left != NULL && x->left->right == NULL){
			temp = x->left->left;
			delete x->left;
			x->left = temp;
			return x;
		}
		else if(x->left->left == NULL && x->left->right != NULL){
			temp = x->left->right;
			delete x->left;
			x->left = temp;
			return x;
		}
		if(x->right->right != NULL && x->right->left == NULL){
			temp = x->right->right;
			delete x->right;
			x->right = temp;
			return x;
		}
		else if(x->right->right == NULL && x->right->left != NULL){
			temp = x->right->left;
			delete x->right;
			x->right = temp;
			return x;
		}
	}
	if(i > x->value) x = Delete(i,x->right,a);
	if(i < x->value) x = Delete(i,x->left,a);

	return a->root;
}

node * AVL::rightRotate(node *a){																			//Right Right case of AVL rotation
	node * x, *y;

    x = a->left;
    y = x->right;
    x->right = a;
    a->left = y;
	x->height = x->height - 1;

	fixHeights(x,x->height);	//fixing heights of sub-trees
	return x;
}

node * AVL::leftRotate(node *a){																			//Left Left case of AVL rotation
	node *x,*y;

	x = a->right;				//doing the rotation
	y = x->left;
	x->left = a;
	a->right = y;
	x->height = x->height - 1;

	fixHeights(x,x->height);		//fixing heights of sub-trees
	return x;
}

node * AVL::leftRightRotate(node *a){																		//Left Right case of AVL rotation

	a->left = leftRotate(a->left);
	return rightRotate(a);
}

node * AVL::rightLeftRotate(node *a){																		//Right Left case of AVL rotation

	a->right = rightRotate(a->right);
	return leftRotate(a);
}

int AVL::treeBalance(node * a, int k){																		//Returns the maximum height of the tree inserted.

	int i = 0 , j = 0;

	if(a!=NULL) i = a->height;
	if(k < i) k = i;

	if(a != NULL){
		if(a->left != NULL){
			i = treeBalance(a->left,k);
			if(i > k && i > j) k = i;
			else if(j > k && j > i) k = j;
		}
		if(a->right != NULL){
			j = treeBalance(a->right,k);
			if(i > k && i > j) k = i;
			else if(j > k && j > i) k = j;
		}
	}
	return k;
}

void AVL::fixHeights(node *a, int i){																		//Fixing heights of every node above a
																											//Works recursively and usually is called after
	a->height = i;																							//a rotation is done.
	if(a->left != NULL) fixHeights(a->left,a->height+1);
	if(a->right != NULL) fixHeights(a->right,a->height+1);
}

int AVL::getsize(AVL * a){																					//Returns the size of AVL object.
	return a->size;
}

int AVL::findMin(AVL *a){																					//Returns the minimum value of all nodes value.
	return a->minimum;
}

string AVL::search(int x, AVL * a){																			//Returns SUCCESS if x is found in AVL object
																											//else FAILURE
	node *c = NULL;

	c = a->root;
	while(c!=NULL){
		if(x == c->value) return "SUCCESS";
		if(compare(x,c->value) && c->right != NULL){
			c = c->right;
			continue;
		}
		else if(compare(x,c->value) && c->right == NULL) return "FAILURE";
		if(!compare(x,c->value) && c->left != NULL){
			c = c->left;
			continue;
		}
		else if(!compare(x,c->value) && c->left == NULL) return "FAILURE";
	}
	return "FAILURE";
}

int AVL::extractNumber(string s){																			//This function is used when we want to insert,delete
																											//or search a number inside an object because we need
	if(!s.find("DELETE") || !s.find("INSERT") || !s.find("SEARCH")){										//to extract the number that is contained in commmand.
		if(s.find("AVLTREE") || s.find("HASHTABLE") || s.find("MAXHEAP") || s.find("MINHEAP")){
			s.erase(0,14);
			return std::stoi(s);
		}
	}
	return 0;
}

int AVL::compare(int x, int y){																				//Compares x and y if x > y returns 1 else 0

	if(x > y) return 1;
	else if(x < y) return 0;
	return 1;
}

node * AVL::createNode(int x){																				//Creates a node pointer using default constructor

	node * p = NULL;

	p = new node(x);

	return p;
}

void AVL::display(int level, node *ptr, AVL * a){															//A display function of the tree.
    int i;
    if (ptr!=NULL)
    {
        display(level + 1, ptr->right, a);
        cout<<endl;
        if (ptr == a->root) cout<<"Root -> ";
        for (i = 0; i < level && ptr != a->root; i++)
            cout<<"        ";
        cout<<ptr->value;
        display(level + 1, ptr->left, a);
    }
}

void AVL::deleteNode(node *a){																				//Delete function for a single node
	a->left = NULL;
	a->right = NULL;
	delete a;
	a = NULL;
}

void AVL::preorder(node *a){																				//Preorder traversing of AVL tree

    if(a != NULL){
    	cout<<"value = "<<a->value<<" Height = "<<a->height<<endl;
        preorder(a->left);
        preorder(a->right);
    }
}

void AVL::deleteALL(node * x){																				//Delete function for every node in the object.

	if(x != NULL){
		deleteALL(x->left);
		deleteALL(x->right);
		deleteNode(x);
		x=NULL;
	}
}


