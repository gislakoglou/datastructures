#ifndef MAXHEAP_H_
#define MAXHEAP_H_
#include "structure.h"

using namespace std;

class MAXHEAP{

    public:
    node * root;
    int size;
    int minimum;
    int capacity;

    MAXHEAP * build(string );
    int getsize(MAXHEAP *);
    node * insert(int ,node *,MAXHEAP *);
    void Delete(int, node *, MAXHEAP *);
    int findMax(MAXHEAP *);

    node * createNode(int );
    int extractNumber(string S);
    void deleteNode(node *);

    void preorder(node *);
    void display(int , node *, MAXHEAP *);
    void swapNodes(node *, node *);
    int defineHeightFromCapacity(int x);
    void fixMaximum(node *);

    void deleteALL(node *);
};

#endif
