#ifndef AVL_H_
#define AVL_H_

#include "structure.h"

using namespace std;

class AVL{

    public:
    node * root;
    int size;
    int minimum;

    AVL* build(string );
    int getsize(AVL *);
    node * insert(int ,node *,AVL *);
    node* Delete(int ,node * ,AVL *);
    int findMin(AVL *);
    string search(int ,AVL *);

    int compare(int ,int );
    node * createNode(int );
    int extractNumber(string S);
    void deleteNode(node *);

    node * rightRotate(node *);
    node * leftRotate(node *);
    node * leftRightRotate(node *);
    node * rightLeftRotate(node *);
    void fixHeights(node *, int);

    void preorder(node *);
    void display(int, node *, AVL *);
    int treeBalance(node *, int );

    void deleteALL(node *);
};

#endif
