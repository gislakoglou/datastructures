#include "MAXHEAP.h"																				//Include all header files and chrono library to measure time
#include "MINHEAP.h"
#include "AVL.h"
#include "GRAPH.h"
#include "HASHTABLE.h"
#include <chrono>

using namespace std;
using namespace std::chrono;

int main(){

    std::fstream file;																				//Declaration of variables and stream names.
    std::ofstream file2;
    std::string line, s, p;

    size_t i = 0;
    int k = 6, j = 0;

    AVL *avl = NULL;
    MINHEAP *minheap = NULL;
    MAXHEAP *maxheap = NULL;
    HASHTABLE *hashtable = NULL;

    high_resolution_clock::time_point t1,t2;

    file.open("commands.txt",ios::in);																//Opening commands.txt for read
    file2.open("output.txt", ios_base::app);														//Opening output.txt for append

    if(!file){																						//Check if streams are opened.
       cerr<<"Error in opening file!!!"<<endl;
       return 0;
    }
    if(!file2){
       cerr<<"Error in opening file!!!"<<endl;
       return 0;
    }

    while (getline(file, line)){
        if(!line.find("BUILD")){																//BUILD section for each structure
            i = strlen(line.c_str());
            if(line.find("MINHEAP")==6){															//Instance: MINHEAP
            	s = line.substr(14,i-15);															//s = string containing file to be opened
            	high_resolution_clock::time_point t1 = high_resolution_clock::now();				//t1 = time the function starts
                minheap = minheap->MINHEAP::build(s);												//calling build to create minheap from file
            	high_resolution_clock::time_point t2 = high_resolution_clock::now();				//t2 = time the function stops
            	if(minheap != NULL) file2<<"Built object MINHEAP from "<<s<<" ";					//writing to output.txt the result of command s
            }
            if(line.find("AVLTREE")==6){															//Instance: AVLTREE
            	s = line.substr(14,i-15);															//same as above with the instance MINHEAP
            	t1 = high_resolution_clock::now();
                avl = avl->AVL::build(s);
                t2 = high_resolution_clock::now();
                if(avl != NULL) file2<<"Built object AVLTREE from "<<s<<" ";
            }
            if(line.find("MAXHEAP")==6){															//Instance: MAXHEAP
            	s = line.substr(14,i-15);															//same as above with the instance MINHEAP
            	t1 = high_resolution_clock::now();
                maxheap = maxheap->MAXHEAP::build(s);
            	t2 = high_resolution_clock::now();
            	if(maxheap != NULL) file2<<"Built object MAXHEAP from "<<s<<" ";
            }
            if(line.find("HASHTABLE")==6){															//Instance: HASHTABLE
            	s = line.substr(16,i-17);															//same as above with the instance MINHEAP
            	t1 = high_resolution_clock::now();
            	hashtable = hashtable->HASHTABLE::build(s);
            	t2 = high_resolution_clock::now();
            	if(hashtable != NULL) file2<<"Built object HASHTABLE from "<<s<<" ";
            }
            if(line.find("GRAPH")==6){																//Instance: GRAPH
            	s = line.substr(12,i-13);															//same as above with the instance MINHEAP
            }
        }
        if(!line.find("GETSIZE")){																//GETSIZE section for each structure
            if(line.find("MINHEAP")==8){															//Instance: MINHEAP
            	t1 = high_resolution_clock::now();
            	j = minheap->MINHEAP::getsize(minheap);												//calls getsize for MINHEAP object
            	t2 = high_resolution_clock::now();
            	if(minheap != NULL) file2<<"SIZE of object MINHEAP = "<<j<<"\n";
            }
            if(line.find("MAXHEAP")==8){															//Instance: MAXHEAP
            	t1 = high_resolution_clock::now();
            	j = maxheap->MAXHEAP::getsize(maxheap);
            	t2 = high_resolution_clock::now();
            	if(maxheap != NULL) file2<<"SIZE of object MAXHEAP = "<<j<<"\n";
            }
            if(line.find("AVLTREE") == 8){															//Instance: AVLTREE
            	t1 = high_resolution_clock::now();
            	j = avl->AVL::getsize(avl);
            	t2 = high_resolution_clock::now();
            	if(avl != NULL) file2<<"SIZE of object AVLTREE = "<<j<<"\n";
            }
            if(line.find("HASHTABLE")==8){															//Instance: HASHTABLE
            	t1 = high_resolution_clock::now();
            	j = hashtable->HASHTABLE::getsize(hashtable);
            	t2 = high_resolution_clock::now();
            	if(hashtable != NULL) file2<<"SIZE of object HASHTABLE = "<<j<<"\n";
            }
            if(line.find("GRAPH")==8){}
        }
        if(!line.find("FINDMIN")){																//FINDMIN section
            if(line.find("MINHEAP")==8){															//Instance: MINHEAP
            	t1 = high_resolution_clock::now();
            	j = minheap->MINHEAP::findMin(minheap);
            	t2 = high_resolution_clock::now();
            	if(minheap != NULL) file2<<"MINIMUM of object MINHEAP = "<<j<<"\n";
            }
            if(line.find("AVLTREE")==8){															//Instance: AVLTREE
            	t1 = high_resolution_clock::now();
            	j = avl->AVL::findMin(avl);
            	t2 = high_resolution_clock::now();
            	if(avl != NULL) file2<<"MINIMUM of object AVLTREE = "<<j<<"\n";
            }
        }
        if(!line.find("FINDMAX")){																//FINDMAX section for MAXHEAP
        	t1 = high_resolution_clock::now();
        	j = maxheap->MAXHEAP::findMax(maxheap);
        	t2 = high_resolution_clock::now();
        	if(maxheap != NULL) file2<<"MAXIMUM of object MAXHEAP = "<<j<<"\n";
        }
        if(!line.find("SEARCH")){																//SEARCH section for HASHTABLE and AVLTREE
            if(line.find("HASHTABLE")==7){
            	k = hashtable->HASHTABLE::extractNumber(line);										//First we save at k the Number that we want to search
            	t1 = high_resolution_clock::now();													//in HASHTABLE object using the extractNumber member function
            	p = hashtable->HASHTABLE::search(k,hashtable);										//Then we send it to search and SUCCESS or FAILURE is returned.
            	t2 = high_resolution_clock::now();
            	file2<<"SEARCH of "<<k<<" in object HASHTABLE = "<<p<<"\n";
            }
            if(line.find("AVLTREE")==7){
            	k = avl->AVL::extractNumber(line);
            	t1 = high_resolution_clock::now();
            	p = avl->AVL::search(k,avl);
            	t2 = high_resolution_clock::now();
            	file2<<"SEARCH of "<<k<<" in object AVLTREE = "<<p<<"\n";
            }
        }
        if(!line.find("COMPUTESHORTESTPATH"))   cout<<"COMPUTESHORTESTPATH"<<endl;
        if(!line.find("COMPUTESPANNINGTREE"))   cout<<"COMPUTESPANNINGTREE"<<endl;
        if(!line.find("FINDCONNECTEDCOMPONENTS"))   cout<<"FINDCONNECTEDCOMPONENTS"<<endl;
        if(!line.find("INSERT")){																//INSERT section for each structure
            if(line.find("AVLTREE")==7){															//Section AVLTREE
            	k = avl->AVL::extractNumber(line);													//We extract the number we want to insert.
            	cout<<"value to be inserted = "<<k<<endl;
            	t1 = high_resolution_clock::now();
            	avl->AVL::insert(k,avl->root,avl);													//We send it to insert to properly get inserted.
            	t2 = high_resolution_clock::now();
            	file2<<"INSERT of "<<k<<" in object AVLTREE \n";
            }
            if(line.find("MINHEAP")==7){															//Section MINHEAP
            	k = minheap->MINHEAP::extractNumber(line);
            	t1 = high_resolution_clock::now();
            	minheap->MINHEAP::insert(k,minheap->root,minheap);
            	t2 = high_resolution_clock::now();
            	file2<<"INSERT of "<<k<<" in object MINHEAP \n";
            }
            if(line.find("MAXHEAP")==7){															//Section MAXHEAP
            	k = maxheap->MAXHEAP::extractNumber(line);
            	t1 = high_resolution_clock::now();
            	maxheap->MAXHEAP::insert(k,maxheap->root,maxheap);
            	t2 = high_resolution_clock::now();
            	file2<<"INSERT of "<<k<<" in object MAXHEAP \n";
            }
            if(line.find("HASHTABLE")==7){															//Section HASHTABLE
            	k = hashtable->HASHTABLE::extractNumber(line);
            	t1 = high_resolution_clock::now();
            	hashtable->HASHTABLE::insert(k,hashtable);
            	t2 = high_resolution_clock::now();
            	file2<<"INSERT of "<<k<<" in object HASHTABLE \n";
            }
            if(line.find("GRAPH")==7){}
            cout<<"INSERT"<<endl;
        }
        if(!line.find("DELETE")){
            if(line.find("AVLTREE")==7){
            	k = avl->AVL::extractNumber(line);
            	if(avl->AVL::search(k,avl) == "SUCCESS"){
            		t1 = high_resolution_clock::now();
            		avl->root = avl->AVL::Delete(k,avl->root,avl);
            		t2 = high_resolution_clock::now();
            	}
            	else file2<<"Node not included! ";
            }
            if(line.find("GRAPH")==7){}
        }
        if(!line.find("DELETEMAX")){
        	k = maxheap->minimum;
        	t1 = high_resolution_clock::now();
        	maxheap->MAXHEAP::Delete(0,maxheap->root,maxheap);
        	t2 = high_resolution_clock::now();
        	file2<<"DELETE of MAXIMUM("<<k<<") in object MAXHEAP ";
        }
        if(!line.find("DELETEMIN")){
        	k = minheap->minimum;
        	t1 = high_resolution_clock::now();
        	minheap->MINHEAP::Delete(0,minheap->root,minheap);
        	t2 = high_resolution_clock::now();
        	file2<<"DELETE of MINIMUM("<<k<<") in object MINHEAP ";
        }
        auto duration = duration_cast<microseconds>( t2 - t1 ).count();								//We compute t2-t1 to get the ammount of time a function used.
        file2<<"Time: "<<duration<<" μsec\n";														//Then we write it to output.txt
    }

    file.close();																					//Closed commands.txt
    file2.close();																					//Closed outputx.txt

    if(avl != NULL){																				//Removing every objects memory that was allocated with new.
        avl->AVL::deleteALL(avl->root);																//By using deleteALL
        delete avl;																					//Then we delete AVL object.
    }
    if(minheap != NULL){
        minheap->MINHEAP::deleteALL(minheap->root);
        delete minheap;
    }
    if(maxheap != NULL){
        maxheap->MAXHEAP::deleteALL(maxheap->root);
        delete maxheap;
    }
    if(hashtable != NULL){
        hashtable->HASHTABLE::deleteALL(hashtable);
        delete hashtable;
    }

    return 0;
}
