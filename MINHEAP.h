#ifndef MINHEAP_H_
#define MINHEAP_H_

#include "structure.h"

using namespace std;

class MINHEAP{

    public:
    node * root;
    int size;
    int minimum;
    int capacity;

    MINHEAP * build(string );
    int getsize(MINHEAP *);
    node * insert(int ,node *,MINHEAP *);
    void Delete(int, node *, MINHEAP *);
    int findMin(MINHEAP *);

    node * createNode(int );
    int extractNumber(string S);
    void deleteNode(node *);

    void preorder(node *);
    void display(int , node *, MINHEAP *);
    void swapNodes(node *, node *);
    int defineHeightFromCapacity(int x);
    void fixMinimum(node *);

    void deleteALL(node *);
};

#endif
