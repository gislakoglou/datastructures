#ifndef STRUCTURE_H_
#define STRUCTURE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;

typedef struct node{
    node * left;
    node * right;
    int value;
    int height;
    node(int x){
    	right = NULL;
    	left = NULL;
    	value = x;
    	height = 0;
    }
}node;

#endif

