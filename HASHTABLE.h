#ifndef HASHTABLE_H_
#define HASHTABLE_H_

#include "structure.h"
#define hash_division 2069

using namespace std;

typedef struct list{
	list *next;
	int value;
}list;

class HASHTABLE{

    public:
    list * root[hash_division];
    int size;

    HASHTABLE * build(string );
    int getsize(HASHTABLE *);
    void insert(int, HASHTABLE *);
    string search(int ,HASHTABLE *);
    int hashFunction(int );

    int extractNumber(string S);

    void display(HASHTABLE *);

    void deleteALL(HASHTABLE *);
};

#endif
