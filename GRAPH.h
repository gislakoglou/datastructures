#ifndef GRAPH_H_
#define GRAPH_H_

#include "structure.h"
#define NMAX 1000

using namespace std;

typedef struct vertice{
	vertice *next[NMAX];		//Just a simple linked list that creates the links between the vertices of the graph.
	int value;					//Value of vertice.
	int degree;					//Degree of vertice similar with possibleEdges.
	int cycle;					//If the vertice is part of a cycle 1 else 0.
}vertice;

typedef struct graph{
	vertice *lists[NMAX];	//We declare an array of pointers of type vertice so that we save the different paths of the graph.
	int possibleEdges;		//Number of edges that the current vertice creates.
}graph;

class GRAPH{

    public:
    int vertices;		//Number of vertices of the graph.
    int edges;			// Number of edges of the graph.
    graph * head[NMAX];	// We declare an array of pointers of type graph to save the different non connected components that might be built.
    int nonConnectedComponents;

    GRAPH * build(string );
    void getsize(GRAPH *);
    string insert(int ,int ,GRAPH *);
    vertice * search(int , graph * , vertice *);

    int computeShortestPath(int , int);
    int computeSpanningTree(GRAPH *);
    int findConnectedComponents(GRAPH *);

    vertice * createNewVertice(int );
    graph * createNewComponent(int , int );
    int extractNumber(string S);
    void findVertice(int , graph *);
    void display(GRAPH *);

};
#endif
