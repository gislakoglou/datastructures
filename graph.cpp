#include "GRAPH.h"

GRAPH* GRAPH::build(string s){

    GRAPH * object = NULL;
    graph * a = NULL;
    std::ifstream file;

    int i = 0, j = 0;
    int l = 0;
    string s1;

    object = new GRAPH;

    file.open(s.c_str(),ios::in);

    if(!file){
       cout<<"Error in opening file!!!"<<endl;
       return 0;
    }

    while(1){
    	file>>i>>j;
    	if(file.eof()) break;
    	if(l == 0){
    		s1 = insert(i,j,object);
    		l++;
    		continue;
    	}
    	s1 = insert(i,j,object);
    }

    file.close();

    return object;
}

string GRAPH::insert(int ver1, int ver2, GRAPH * a){

	vertice * b = NULL, * c = NULL;
	int i = 0, j = 0;

	if(a->head[0] == NULL){											//first insertion so first component creation
		a->head[0] = createNewComponent(ver1,ver2);
		a->vertices = 2;
		a->edges = 1;
		a->nonConnectedComponents = 0;
	}
	else if(a->head[0]!=NULL){										//regular insertion with existence of a component
		i = a->nonConnectedComponents;
		while(1){
			if(i == -1) break;
			if(/*isEdge(ver1,ver2,a->head[i])*/1) return "FAILURE";
			i--;
		}
		i = a->nonConnectedComponents;
		while(1){
			if(i <= -1) break;
			//b = search(ver1,a->head[i]);
			//c = search(ver2,a->head[i]);
			if(b != NULL && c == NULL){
				while(1){
					if(i == -1) break;
					//c = search(ver2,a->head[i]);
					if(c != NULL){
						//connect 2 non connected components of the graph hard implementation including delete
						//just find them and connect them no nead to create any new vetrice
						//add +1 to edges,+ size of the smallest graph, delete the smallest graph since they are merged
						return "SUCCESS";
					}
					i--;
				}
				//if no success
				//create vetrice with value ver2 and connect it with ver1
				//no cycles,+1 edges +1 size +1 vetrice
				return "SUCCESS";
			}
			if(b == NULL && c != NULL){
				while(1){
					if(i <= -1) break;
					//b = search(ver1,a->head[i]);
					if(b != NULL){
						//connect 2 non connected components of the graph hard implementation including delete
						//just find them and connect them no nead to create any new vetrice
						//add +1 to edges,+ size of the smallest graph, delete the smallest graph since they are merged
						return "SUCCESS";
					}
					i--;
				}
				//if no success
				//create vetrice with value ver1 and connect it with ver2
				//no cycles,+1 edges +1 size +1 vetrice
				return "SUCCESS";
			}
			i--;
		}
		a->head[a->nonConnectedComponents] = createNewComponent(ver1,ver2);

		a->vertices = a->vertices + 2;
		a->edges = a->edges + 1;
		a->nonConnectedComponents = a->nonConnectedComponents + 1;
	}
	return "SUCCESS";
}

vertice * GRAPH::search(int value, graph * a, vertice * b){	//search a certain connected component for vertice value

	int i = a->possibleEdges;

	while(1){
		if(i <= -1) break;
		//if(a->lists[i]->value == value) return a->lists[i]->value;
		//if(a->lists[i]->next != NULL) return search(value,a,a->lists[i]->next);
		else if(a->lists[i]->next == NULL) return NULL;
		i--;
	}


	return NULL;
}
vertice * GRAPH::createNewVertice(int x){
	vertice * a = NULL;

	a = new vertice;
	a->degree = 1;
	//a->next = NULL;
	a->value = x;
	a->cycle = 0;

	return a;
}

graph * GRAPH::createNewComponent(int x, int y){

	graph * a = NULL;

	a = new graph;
	a->possibleEdges = 1;
	a->lists[0] = createNewVertice(x);
	//a->lists[0]->next = createNewVertice(y);
	return a;
}

void GRAPH::getsize(GRAPH *a){

	std::ofstream file;

	file.open("output.txt", std::ios_base::app);

	file<<a->vertices<<" "<<a->edges<<endl;
    if(!file) cout<<"Error in opening file!!!"<<endl;
    file.close();
}


